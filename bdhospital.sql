-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-05-2016 a las 01:24:53
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bdhospital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `id_administrador` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `fkid_estado` bigint(20) NOT NULL,
  `fkid_hospital` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_administrador`),
  KEY `estadoAdministrador` (`fkid_estado`),
  KEY `hospitalAdministrador` (`fkid_hospital`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_administrador`, `nombre`, `rol`, `fkid_estado`, `fkid_hospital`) VALUES
(1, 'jhon edison', 'operario', 1, 2),
(2, 'amanda monroy', 'operaria', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE IF NOT EXISTS `cita` (
  `id_cita` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `fkid_enfermero` bigint(20) NOT NULL,
  `fkid_paciente` bigint(20) NOT NULL,
  `fkid_estado` bigint(20) DEFAULT NULL,
  `fkid_administrador` bigint(20) DEFAULT NULL,
  `fecha_solicitud` date DEFAULT NULL,
  PRIMARY KEY (`id_cita`),
  KEY `citaAsignata` (`fkid_enfermero`),
  KEY `PacienteAsignado` (`fkid_paciente`),
  KEY `estadoCita` (`fkid_estado`),
  KEY `ProgramadorCitas` (`fkid_administrador`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `cita`
--

INSERT INTO `cita` (`id_cita`, `fecha`, `fkid_enfermero`, `fkid_paciente`, `fkid_estado`, `fkid_administrador`, `fecha_solicitud`) VALUES
(1, '2016-05-11 00:00:00', 1, 1, 1, 2, '2016-05-18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE IF NOT EXISTS `ciudad` (
  `id_ciudad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_ciudad` varchar(50) DEFAULT NULL,
  `fkid_estado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_ciudad`),
  KEY `estadoCiudad` (`fkid_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id_ciudad`, `nombre_ciudad`, `fkid_estado`) VALUES
(1, 'bogota', 1),
(2, 'medellin', 1),
(3, 'cali', 1),
(4, 'amazonas', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico`
--

CREATE TABLE IF NOT EXISTS `diagnostico` (
  `id_diagnostico` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `fkid_enfermero` bigint(20) NOT NULL,
  `fkid_paciente` bigint(20) NOT NULL,
  `fecha` date DEFAULT NULL,
  `fkid_historial` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_diagnostico`),
  KEY `EnfermeroDiagnosticador` (`fkid_enfermero`),
  KEY `PacienteDiagnosticado` (`fkid_paciente`),
  KEY `diagnosticoIngresado` (`fkid_historial`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `diagnostico`
--

INSERT INTO `diagnostico` (`id_diagnostico`, `descripcion`, `fkid_enfermero`, `fkid_paciente`, `fecha`, `fkid_historial`) VALUES
(1, 'el paciente llego con poseido por la peresa', 1, 1, '2016-05-18', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermero`
--

CREATE TABLE IF NOT EXISTS `enfermero` (
  `id_enfermero` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fkid_estado` bigint(20) DEFAULT NULL,
  `fkid_hospital` bigint(20) DEFAULT NULL,
  `especialidad` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id_enfermero`),
  KEY `estadoEnfermero` (`fkid_estado`),
  KEY `HospitalEnfermero` (`fkid_hospital`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `enfermero`
--

INSERT INTO `enfermero` (`id_enfermero`, `nombre`, `fkid_estado`, `fkid_hospital`, `especialidad`) VALUES
(1, 'jhon edison', NULL, NULL, NULL),
(2, 'mateo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `id_estado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `estado`
--

INSERT INTO `estado` (`id_estado`, `nombre`) VALUES
(1, 'activo'),
(2, 'inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE IF NOT EXISTS `historial` (
  `id_historial` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) DEFAULT NULL,
  `fkid_paciente` bigint(20) DEFAULT NULL,
  `nombre_paciente` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_historial`),
  KEY `paciente` (`fkid_paciente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `historial`
--

INSERT INTO `historial` (`id_historial`, `descripcion`, `fkid_paciente`, `nombre_paciente`) VALUES
(1, 'en estado de mejora', 1, 'jhon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hospital`
--

CREATE TABLE IF NOT EXISTS `hospital` (
  `id_hospital` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_hospital` varchar(50) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `fkid_ciudad` bigint(20) DEFAULT NULL,
  `fkid_administrador` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_hospital`),
  KEY `ciudadHospital` (`fkid_ciudad`),
  KEY `administradorHospital` (`fkid_administrador`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `hospital`
--

INSERT INTO `hospital` (`id_hospital`, `nombre_hospital`, `direccion`, `fkid_ciudad`, `fkid_administrador`) VALUES
(2, 'cafesalud', 'calle falsa 123', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_medicamento`
--

CREATE TABLE IF NOT EXISTS `inventario_medicamento` (
  `id_inventario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_medicamento` varchar(50) DEFAULT NULL,
  `stock_minimo` bigint(20) DEFAULT NULL,
  `stock_maximo` bigint(20) DEFAULT NULL,
  `fecha_salida` date DEFAULT NULL,
  PRIMARY KEY (`id_inventario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `inventario_medicamento`
--

INSERT INTO `inventario_medicamento` (`id_inventario`, `nombre_medicamento`, `stock_minimo`, `stock_maximo`, `fecha_salida`) VALUES
(1, 'acetaminofen', 100, 100000, '2016-05-05'),
(2, 'dolex', 5, 5000, '2016-05-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamento`
--

CREATE TABLE IF NOT EXISTS `medicamento` (
  `id_medicamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `fkid_diagnostico` bigint(20) DEFAULT NULL,
  `fkid_estado` bigint(20) DEFAULT NULL,
  `cantidad` bigint(20) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fkid_inventario` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_medicamento`),
  KEY `MedicamentoAsignado` (`fkid_diagnostico`),
  KEY `estadoMedicamento` (`fkid_estado`),
  KEY `inventarioMedicamento` (`fkid_inventario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=9 ;

--
-- Volcar la base de datos para la tabla `medicamento`
--

INSERT INTO `medicamento` (`id_medicamento`, `nombre`, `descripcion`, `fkid_diagnostico`, `fkid_estado`, `cantidad`, `fecha_ingreso`, `fkid_inventario`) VALUES
(8, 'acetaminofen', 'sirve para todo', 1, 1, 5, '2016-05-10', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
  `id_paciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fkid_estado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_paciente`),
  KEY `estadoPaciente` (`fkid_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id_paciente`, `nombre`, `fkid_estado`) VALUES
(1, 'jhon edison', NULL);

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD CONSTRAINT `administrador_ibfk_2` FOREIGN KEY (`fkid_hospital`) REFERENCES `hospital` (`id_hospital`),
  ADD CONSTRAINT `administrador_ibfk_1` FOREIGN KEY (`fkid_estado`) REFERENCES `estado` (`id_estado`);

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`fkid_enfermero`) REFERENCES `enfermero` (`id_enfermero`),
  ADD CONSTRAINT `cita_ibfk_2` FOREIGN KEY (`fkid_paciente`) REFERENCES `paciente` (`id_paciente`),
  ADD CONSTRAINT `cita_ibfk_3` FOREIGN KEY (`fkid_estado`) REFERENCES `estado` (`id_estado`),
  ADD CONSTRAINT `cita_ibfk_4` FOREIGN KEY (`fkid_administrador`) REFERENCES `administrador` (`id_administrador`);

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`fkid_estado`) REFERENCES `estado` (`id_estado`);

--
-- Filtros para la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD CONSTRAINT `diagnostico_ibfk_3` FOREIGN KEY (`fkid_historial`) REFERENCES `historial` (`id_historial`),
  ADD CONSTRAINT `diagnostico_ibfk_1` FOREIGN KEY (`fkid_enfermero`) REFERENCES `enfermero` (`id_enfermero`),
  ADD CONSTRAINT `diagnostico_ibfk_2` FOREIGN KEY (`fkid_paciente`) REFERENCES `paciente` (`id_paciente`);

--
-- Filtros para la tabla `enfermero`
--
ALTER TABLE `enfermero`
  ADD CONSTRAINT `enfermero_ibfk_1` FOREIGN KEY (`fkid_estado`) REFERENCES `estado` (`id_estado`),
  ADD CONSTRAINT `enfermero_ibfk_2` FOREIGN KEY (`fkid_hospital`) REFERENCES `hospital` (`id_hospital`);

--
-- Filtros para la tabla `historial`
--
ALTER TABLE `historial`
  ADD CONSTRAINT `historial_ibfk_2` FOREIGN KEY (`fkid_paciente`) REFERENCES `paciente` (`id_paciente`);

--
-- Filtros para la tabla `hospital`
--
ALTER TABLE `hospital`
  ADD CONSTRAINT `hospital_ibfk_2` FOREIGN KEY (`fkid_administrador`) REFERENCES `administrador` (`id_administrador`),
  ADD CONSTRAINT `hospital_ibfk_1` FOREIGN KEY (`fkid_ciudad`) REFERENCES `ciudad` (`id_ciudad`);

--
-- Filtros para la tabla `medicamento`
--
ALTER TABLE `medicamento`
  ADD CONSTRAINT `medicamento_ibfk_4` FOREIGN KEY (`fkid_inventario`) REFERENCES `inventario_medicamento` (`id_inventario`),
  ADD CONSTRAINT `medicamento_ibfk_1` FOREIGN KEY (`fkid_diagnostico`) REFERENCES `diagnostico` (`id_diagnostico`),
  ADD CONSTRAINT `medicamento_ibfk_2` FOREIGN KEY (`fkid_estado`) REFERENCES `estado` (`id_estado`),
  ADD CONSTRAINT `medicamento_ibfk_3` FOREIGN KEY (`fkid_diagnostico`) REFERENCES `diagnostico` (`id_diagnostico`);

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `paciente_ibfk_1` FOREIGN KEY (`fkid_estado`) REFERENCES `estado` (`id_estado`);
